//
//  Device.swift
//  RestApiTest
//
//  Created by Mackwell on 22/01/2016.
//  Copyright © 2016 Mackwell. All rights reserved.
//

import Foundation


struct Device {
    var image:      String
    var address:    Int
    var loop:       Int
    var name:       String
    var status:     String
    var statusColor:String
    var modal:      String
    var serialNo:   String
    var gtinNo:     String
    
    var circuitF:   Int
    var batteryDF:  Int
    var batteryF:   Int
    var lampF:      Int
    var ftDelay:    Int
    var dtDelay:    Int
    var ftf:        Int
    var dtf:        Int
    var inhibit:    Int
    var ftd:        Int
    var dtd:        Int
    var batteryC:   Int
    var ftp:        Int
    var dtp:        Int
    var restM:      Int
    var normalM:    Int
    var emerM:      Int
    var eemerM:     Int
    var ft:         Int
    var dt:         Int
    
    init(name: String, circuitF: Int) {
        self.image              = ""
        self.address            = 0
        self.loop               = 0
        self.name               = name
        self.status             = ""
        self.statusColor        = ""
        self.modal              = ""
        self.serialNo           = "1234567890"
        self.gtinNo             = "33333333333"
        
        self.circuitF           = circuitF
        self.batteryDF          = 0
        self.batteryF           = 0
        self.lampF              = 0
        self.ftDelay            = 0
        self.dtDelay            = 0
        self.ftf                = 0
        self.dtf                = 0
        self.inhibit            = 0
        self.ftd                = 0
        self.dtd                = 0
        self.batteryC           = 0
        self.ftp                = 0
        self.dtp                = 0
        self.restM              = 0
        self.normalM            = 0
        self.emerM              = 0
        self.eemerM             = 0
        self.ft                 = 0
        self.dt                 = 0
        
        
        
        
    }
    
    func toJson() -> [String: AnyObject] {
        let props: [String: AnyObject] = [
            "Image":        self.image,
            "Address":      self.address,
            "Loop":         self.loop,
            "Name":         self.name,
            "Status":       self.status,
            "StatusColour": self.statusColor,
            "Modal":        self.modal,
            "SerialNo":     self.serialNo,
            "GtinNo":       self.gtinNo,
            "CircuritF":    self.circuitF,
            "BatteryDF":    self.batteryDF,
            "BatteryF":     self.batteryF,
            "LampF":        self.lampF,
            "FTDelay":      self.ftDelay,
            "DTDelay":      self.dtDelay,
            "FTF":          self.ftf,
            "DTF":          self.dtf,
            "Inhibit":      self.inhibit,
            "FTD":          self.ftd,
            "DTD":          self.dtd,
            "BatteryC":     self.batteryC,
            "FTP":          self.ftp,
            "DTP":          self.dtp,
            "RestM":        self.restM,
            "NormalM":      self.normalM,
            "EmerM":        self.emerM,
            "EEmerM":       self.eemerM,
            "FT":           self.ft,
            "DT":           self.dt,
        ]
        
        return props
        
    }
    
}
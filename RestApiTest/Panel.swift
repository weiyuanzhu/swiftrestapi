//
//  Panel.swift
//  RestApiTest
//
//  Created by Mackwell on 22/01/2016.
//  Copyright © 2016 Mackwell. All rights reserved.
//

import Foundation

struct Panel {
    var macAddress:     String
    var ipAddress:      String
    var location:       String
    var noOfDevices:    Int
    var status:         String
    var statusColour:   String
    var serialNo:       String
    var gtinNo:         String
    var devices:        [Device]
    
    init (devices: [Device]) {
        self.macAddress = ""
        self.ipAddress = ""
        self.location = ""
        self.status = ""
        self.statusColour = ""
        self.serialNo = ""
        self.gtinNo = ""
        
        self.devices = devices
        self.noOfDevices = devices.count

    }
    
}
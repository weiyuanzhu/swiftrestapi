//
//  User.swift
//  RestApiTest
//
//  Created by Mackwell on 22/01/2016.
//  Copyright © 2016 Mackwell. All rights reserved.
//

import Foundation

struct User {
    var userName:       String
    var loggedIn:       String
    var panels:         [Panel]
    
    init(name: String, loggedIn: String, panels: [Panel]) {
        self.userName = name
        self.loggedIn = loggedIn
        self.panels = panels
    }
    
}

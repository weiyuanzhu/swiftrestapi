//
//  AddTodoViewController.swift
//  RestApiTest
//
//  Created by Mackwell on 20/01/2016.
//  Copyright © 2016 Mackwell. All rights reserved.
//

import UIKit

class AddTodoViewController: UIViewController,UITextFieldDelegate  {

    @IBOutlet weak var todoTextField: UITextField!
    var name: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        todoTextField.resignFirstResponder()
        if segue.identifier == "SaveTodo" {
            name = todoTextField.text
        }
    }


}

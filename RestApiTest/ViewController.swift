//
//  ViewController.swift
//  RestApiTest
//
//  Created by Mackwell on 15/01/2016.
//  Copyright © 2016 Mackwell. All rights reserved.
//

import UIKit


class ViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var ipLabel: UILabel!
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBAction func add(sender: AnyObject) {
        postDataToURL()
    }
    
    
    
    @IBAction func refresh(sender: AnyObject) {
        updateList()
    }
    
    var jsonArray: NSArray = []
    var selected: Int = 0
    var json: String?
    
    let testDevice: Device = Device(name: "TestDevice", circuitF: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateList()
        
        let device = Device(name: "1",circuitF: 0)
        let device2  = Device(name: "2",circuitF: 0)
        let panel = Panel(devices: [device,device2])
        let user = User(name: "admin", loggedIn: "yes", panels: [panel])
        json = JSONSerializer.toJson(user)
        
        print(json)
        
        //        postDataToURL()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateList() {
//        let postEndpoint: String = "https://httpbin.org/ip"
        
//        let postEndpoint: String = "http://jsonplaceholder.typicode.com/posts/1"
        
        let postEndpoint: String = "http://192.168.0.48:8080/todos/"
        let session = NSURLSession.sharedSession()
        let url = NSURL(string: postEndpoint)!
        
        session.dataTaskWithURL(url, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            

            
            guard let realResponse = response as? NSHTTPURLResponse where realResponse.statusCode == 200 else {
                print("Response")
                return
            }
            


            
            let jsonDictionary: NSDictionary

            do {
                self.jsonArray = try NSJSONSerialization.JSONObjectWithData(responseData, options: .MutableContainers) as! NSArray
                jsonDictionary = self.jsonArray[0] as! NSDictionary
            } catch {
                print("Error pasrsing")
                return
            }
            
            print(jsonDictionary.description)
            let origin = jsonDictionary["name"] as! String
            self.performSelectorOnMainThread("updataIPlabel:", withObject: origin, waitUntilDone: false)
            
        
        
        }).resume()
        
        
    }
    
    func postDataToURL() {
//        let postEndpoint: String = "http://requestb.in/vvm7b4vv"
//        let postEndpoint: String = "http://192.168.0.48:8080/todos"
        let postEndpoint: String = "http://192.168.0.48:8080/test1"
        
        let url = NSURL(string: postEndpoint)!
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession.init(configuration: config)
//        let postParams: [String: AnyObject] = ["username": "mobile"]
        let postParams = json
        print(postParams)
        
//        let postParams = testDevice.toJson()
        
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = NSDATA() json
        
//        do {
//            request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(postParams!, options: NSJSONWritingOptions())
//            
//        } catch {
//            print("json serialization error")
//        }
        
        session.dataTaskWithRequest(request, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            

            guard let realResponse = response as? NSHTTPURLResponse where realResponse.statusCode == 201  else {
                
                print("Not a 201 response")
                return
            }
            
            
            if let postString = NSString(data: data!, encoding: NSUTF8StringEncoding) as? String {
                print("POST: " + postString)
                self.performSelectorOnMainThread("updatePostLabel:", withObject: postString, waitUntilDone: false)
            }
        }).resume()
    }
    
    func updataIPlabel(text: String) {
        tableView.reloadData()
    }
    
    func updatePostLabel(text: String) {

    }
    
    
    // MARK: Table View Delegates
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell")!
        
        let jsonDictionary = jsonArray[indexPath.row]
        
        if let name = jsonDictionary["name"] as? String{
            cell.textLabel?.text = name
        } else {
            cell.textLabel?.text = "n/a"
            
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        selected = indexPath.row
        
        performSegueWithIdentifier("ShowTodo", sender: self)
        
    }
    
    // MARK: Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let dest = segue.destinationViewController as? TodoViewController {
            dest.jsonDictionary = jsonArray[selected] as! NSDictionary
            
        }
    }
    
    @IBAction func cancelToTodoList(segue: UIStoryboardSegue) {
    
    }
    
    
    @IBAction func saveTodot(segue: UIStoryboardSegue) {
//        if let sourse = segue.sourceViewController as? AddTodoViewController{
////            let name = sourse.todoTextField.text
////            postDataToURL(name!)
//        }
        
        

    }

}


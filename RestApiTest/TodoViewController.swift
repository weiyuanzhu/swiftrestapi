//
//  TodoViewController.swift
//  RestApiTest
//
//  Created by Mackwell on 19/01/2016.
//  Copyright © 2016 Mackwell. All rights reserved.
//

import UIKit

class TodoViewController: UIViewController {
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dueLabel: UILabel!
    
    
    var jsonDictionary: NSDictionary = ["":""]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(jsonDictionary.description)
        
        idLabel.text = "id: \(jsonDictionary["id"]!)"
        nameLabel.text = "name: \(jsonDictionary["name"]!)"
        statusLabel.text = "Completed: \(jsonDictionary["completed"]!)"
        dueLabel.text = "due: \(jsonDictionary["due"]!)"

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
